import { Component, OnInit } from '@angular/core';
import { Main } from '../interfaces/main';
import { MainService } from '../main.service';
import { Comments } from '../interfaces/comments';
import { Alert } from 'selenium-webdriver';

@Component({
  selector: 'app-mains',
  templateUrl: './mains.component.html',
  styleUrls: ['./mains.component.css']
})

export class MainsComponent implements OnInit {

  //geting posts from json not from db
  mains$: Main[];
  comments$:Comments[];


  constructor(private mainService: MainService) { }

  ngOnInit() {
    //geting posts from json
        this.mainService.getMains().subscribe(data => this.mains$ = data);
        this.mainService.getComments().subscribe(data => this.comments$ = data)

  }

  savePost(id:number, title:string, body:string){
    this.mainService.addPost(id,title,body);
    alert("Saved for later viewing");
  }

}

