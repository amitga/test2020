import { ImageService } from './../image.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { ClassifiedService } from '../classified.service';


@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  category:string = "Loading...";
  categoryImage:string; 
  cat:string;//A variable that will accept the classification result by default
  constructor(public classifyService:ClassifyService,
              public classifiedService:ClassifiedService,
              public imageService:ImageService) {}

/** 
name_to_num(cat:string){//Converts category name to a number
  if (cat == 'business'){
    this.categoryImage = this.imageService.images[0];
    }
    else if (cat == 'entertainment'){
    this.categoryImage = this.imageService.images[1];
    }
    else if (cat == 'politics'){
    this.categoryImage = this.imageService.images[2];
    }
    else if (cat == 'sport'){
    this.categoryImage = this.imageService.images[3];
    }
    else (cat == 'tech'){
      this.categoryImage = this.imageService.images[4];
      }
      return this.categoryImage
    }
*/

 /*method that save the article + his classified + his classified logo to the db */
 myFunc(cat:string){
/**   *
 * let cat_image= name_to_num(this.cat.)
  this.categoryImage = this.imageService.images[cat_image];
  */
  
  if (cat == 'business'){
    this.categoryImage = this.imageService.images[0];
    }
    else if (cat == 'entertainment'){
    this.categoryImage = this.imageService.images[1];
    }
    else if (cat == 'politics'){
    this.categoryImage = this.imageService.images[2];
    }
    else if (cat == 'sport'){
    this.categoryImage = this.imageService.images[3];
    }
    else if (cat == 'tech'){
      this.categoryImage = this.imageService.images[4];
      }
      
  this.classifiedService.addArticle(cat,this.classifyService.doc, this.categoryImage);

// this.ans ="The data retention was successful"
}

  ngOnInit() {
    this.classifyService.classify().subscribe(
      res => {
        console.log(res);
        console.log(this.classifyService.categories[res])
        this.category = this.classifyService.categories[res];
        console.log(this.imageService.images[res]);
        this.categoryImage = this.imageService.images[res];
        console.log(this.classifyService.doc)
        this.cat =this.category//A variable that will accept the classification result by default
      }
    )
  }
 

}
