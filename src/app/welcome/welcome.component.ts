import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(public authService:AuthService ) { }

  userId:string;
  users$:Observable<any>;
  
  ngOnInit() {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.email;
      }
    )
  }

}
  

 

