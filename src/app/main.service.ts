import { Main } from './interfaces/main';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { Router, RouterModule } from '@angular/router';
import { Comments } from './interfaces/comments';


@Injectable({
  providedIn: 'root'
})
export class MainService {

  apiUrl='https://jsonplaceholder.typicode.com/posts/';
  apiUrl_comments='https://jsonplaceholder.typicode.com/posts/1/comments';


  constructor(private http: HttpClient, private db: AngularFirestore,public router:Router) { }


/* GETING FROM JSON*/
getMains(){
  return this.http.get<Main[]>(this.apiUrl)
}
getComments(){
  return this.http.get<Comments[]>(this.apiUrl_comments)
}

addPost(id:number, title:string, body:string,){
  const post = {id:id, title:title, body:body}
  this.db.collection('posts').add(post)  
  this.router.navigate(['/mains']);
}

// addArticle(category:String, body:String, img:string){
//   const article = {category:category, body:body, img:img}
//   this.db.collection('articles').add(article)  
//   this.router.navigate(['/articles']);
// }

}


