import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { GeneralsComponent } from './generals/generals.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { GeneralformComponent } from './generalform/generalform.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ClassifiedComponent } from './classified/classified.component';
import { DocformComponent } from './docform/docform.component';
import { HttpClientModule } from '@angular/common/http';
import { WelcomeComponent } from './welcome/welcome.component';
import { ArticlesComponent } from './articles/articles.component';
import { MainsComponent } from './mains/mains.component';
import {MatDatepickerModule} from '@angular/material/datepicker'; //datepicker
import {MatSelectModule} from '@angular/material/select';
import { DatePipe } from '@angular/common';//
import { MatInputModule, MatNativeDateModule } from '@angular/material';



const appRoutes: Routes = [
    { path: 'generals', component: GeneralsComponent },
    { path: '',
      redirectTo: '/generals',
      pathMatch: 'full'
    },
    { path: 'generalform', component: GeneralformComponent},
    { path: 'generalform/:id', component: GeneralformComponent},
    { path: 'signup', component: SignUpComponent},
    { path: 'login', component: LoginComponent},
    { path: 'classify', component: DocformComponent},
    { path: 'classified', component: ClassifiedComponent},
    { path: 'welcome', component: WelcomeComponent},
    { path: 'articles', component: ArticlesComponent},
    { path: 'mains', component: MainsComponent},
    

    ];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    GeneralsComponent,
    GeneralformComponent,
    LoginComponent,
    SignUpComponent,
    ClassifiedComponent,
    DocformComponent,
    WelcomeComponent,
    ArticlesComponent,
    MainsComponent,
    
  ],
  imports: [
    MatExpansionModule,
    MatCardModule,
    RouterModule.forRoot(
            appRoutes,
            { enableTracing: true } // <-- debugging purposes only
          ),  
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatSelectModule,
    MatNativeDateModule,

    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    AngularFireModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'test20-5fa0d'),
    HttpClientModule,
  ],
  providers: [AngularFireAuth, AngularFirestore,DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }

