import { AuthService } from './../auth.service';
import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Location } from "@angular/common";
import { Router } from "@angular/router";


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {

  title: string = 'Generals';
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, 
              public authService:AuthService,
              location: Location, 
              router: Router){
    router.events.subscribe(val => {
      if (location.path() == "/generals" || location.path() == "/generalform") {
        this.title = 'Saved posts';      
      } else if (location.path() == "/signup"){
        this.title = "Sign up form";
      } else if (location.path() == "/login"){
        this.title = "login form";
     } else if (location.path() == "/classify"){
        this.title = "Classify";
      } else if (location.path() == "/classified"){
        this.title = "Classify";
      } else if (location.path() == "/welcome"){
        this.title = "Welcome";  
      } else if (location.path() == "/articles"){
        this.title = "Articles";  
      } else if (location.path() == "/mains"){
        this.title = "Blog posts"; 
       } else {
        this.title = "Generals";
       
      }
    });   
  }
}
